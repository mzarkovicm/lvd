CREATE TABLE users (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE pages (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `title` VARCHAR(255),
  `body` TEXT,
  `img_path` VARCHAR(255),
  `permalink` VARCHAR(255),
  `date_created` DATETIME,
  `published` BOOLEAN DEFAULT FALSE,
  `order` INT(3)
);

ALTER TABLE users 
ADD COLUMN `order` INT(11) NULL DEFAULT 0 AFTER `password`;

ALTER TABLE `uad`.`users` 
ADD COLUMN `name` VARCHAR(255) NULL AFTER `order`,
ADD COLUMN `email` VARCHAR(255) NULL AFTER `name`;

INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1002, 'bjacobs1', 'duYfSd', 0, 'Benjamin', 'byoung1@geocities.jp');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1003, 'aelliott2', 'kgMDCk2vi', 0, 'Anthony', 'abradley2@163.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1004, 'parnold3', 'EBHBc5epbl', 0, 'Patrick', 'pwheeler3@hibu.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1005, 'whayes4', '8ZqEvg5', 0, 'Wayne', 'wfuller4@people.com.cn');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1006, 'lwood5', '7W7cw5E76KTM', 0, 'Louis', 'lfreeman5@ebay.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1007, 'bgarcia6', 'LId1Gwn', 0, 'Barbara', 'bolson6@wufoo.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1008, 'cmills7', 'dTCSChQli', 0, 'Chris', 'cknight7@youtu.be');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1009, 'mhenderson8', 'il72vhLBK9', 0, 'Martha', 'mmoreno8@auda.org.au');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1010, 'jclark9', 'tnlnNFtNDAu', 0, 'Joe', 'jweaver9@i2i.jp');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1011, 'kkennedya', 'wkcaH3326EN0', 0, 'Karen', 'khenrya@google.com.br');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1012, 'rporterb', 'HC4ih4eMGF3', 0, 'Ryan', 'rlewisb@qq.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1013, 'dkennedyc', 'qquekoqSzJb', 0, 'David', 'ddixonc@sakura.ne.jp');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1014, 'mrossd', 'klUNrS', 0, 'Michael', 'mrobertsond@archive.org');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1015, 'mfullere', '1vZvcyDARn', 0, 'Martha', 'mhenrye@fda.gov');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1016, 'hgomezf', 'QTmhwKixLpzB', 0, 'Harold', 'hreyesf@ocn.ne.jp');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1017, 'jturnerg', '2GROVwUgOAEB', 0, 'Janet', 'jjohnstong@ca.gov');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1018, 'ngibsonh', 'CAKhEpMRs8dT', 0, 'Norma', 'nalvarezh@hatena.ne.jp');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1019, 'criverai', 'l2VcgcVEq0u', 0, 'Cheryl', 'cpetersi@indiatimes.com');
INSERT INTO users (`id`, `username`, `password`, `order`, `name`, `email`) VALUES (1020, 'jharperj', 'soBuzninnOu1', 0, 'Joe', 'jmarshallj@zimbio.com');

CREATE TABLE chats (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `sender_id` INT(11),
  `receiver_id` INT(11),
  `chat` TEXT,
  `read` BOOLEAN DEFAULT FALSE,
  `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,,
  PRIMARY KEY (`id`));

ALTER TABLE `uad`.`chats`
CHARACTER SET = utf8 ;

ALTER TABLE `uad`.`chats`
CHANGE COLUMN `chat` `chat` TEXT CHARACTER SET 'utf8' NULL DEFAULT NULL ;

