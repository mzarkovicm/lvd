<?php

namespace Lamework\Controller\Api;

require_once(BASEPATH . '/vendor/autoload.php');

use \Firebase\JWT\JWT;

class Controller extends \Lamework\Controller\Core\Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->printJSON("Welcome to the Lamework API");
	}

	public function notFound() {
        $this->printJSON("Not found", 404);
    }

	/**
	 * Load a page with JSON result
	 * @param string $result
	 * @param int $status_code
	 * @throws \Exception
	 */
	protected function printJSON($result = "", $status_code = 200) {
        $this->to_tpl['result'] = json_encode($result);
        header('Content-Type: application/json', true, $status_code);
        $this->load_template("json-result");
        exit;
	}

	protected function checkAuthorizationHeaders($refresh = false) {
		$authHeader = $this->getHeader('Authorization');

		/*
		 * Look for the 'authorization' header
		 */
		if ($authHeader) {
			/*
			 * Extract the jwt from the Bearer
			 */
			list($jwt) = sscanf($authHeader, 'Bearer %s');

			if ($jwt) {
				try {
					/*
					 * decode the jwt using the key from config
					 */
					$secretKey = base64_decode(JWTKEY);

                    $token = JWT::decode($jwt, $secretKey, array('HS512'));

					return $token->data;

				} catch (\Firebase\JWT\ExpiredException $e) {
				    if ($refresh) {
                        $this->printJSON('Expired refresh token', 401);
                    }
                    $this->printJSON('Expired access token', 401);
                } catch (Exception $e) {
					/*
					 * the token was not able to be decoded.
					 * this is likely because the signature was not able to be verified (tampered token)
					 */
                    $this->printJSON('HTTP/1.0 401 Unauthorized', 401);
				}
			} else {
				/*
				 * No token was able to be extracted from the authorization header
				 */
                $this->printJSON('HTTP/1.0 400 Bad Request', 400);
			}
		} else {
			/*
			 * The request lacks the authorization token
			 */
			$this->printJSON('HTTP/1.0 400 Bad Request', 400);
			//echo 'Token not found in request';
		}
	}

	private function getHeader($header = '') {
		if ($header) {
			$headers = getallheaders();
			return $headers[$header];
		}
		return false;
	}
}
