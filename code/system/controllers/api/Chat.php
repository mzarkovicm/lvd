<?php

namespace Lamework\Controller\Api;

use Lamework\Model\ChatCollection;

class Chat extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getUserConversationById()
	{
        $token_data = $this->checkAuthorizationHeaders();
        $request_body = file_get_contents('php://input');
        $data = json_decode($request_body);

        $user_id = $data->id;
		$auth_user_id = $token_data->user_id;
		$chats = new ChatCollection();
		$chats->fetchAllByFieldsValues(['sender_id', 'receiver_id'], [[$user_id, $auth_user_id], [$user_id, $auth_user_id]]);
		$payload = [
			'data' => $chats->list
		];
		$this->printJSON($payload);
	}
}
