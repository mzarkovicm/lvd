<?php

namespace Lamework\Controller\Api;

use Lamework\Model\UserCollection;

class User extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getAuthorizedUser()
	{
	    $token_data = $this->checkAuthorizationHeaders();
		$user = new \Lamework\Model\User();
		$user->getById($token_data->user_id);
		$safe_user = [
			'username' => $user->username,
			'user_id' => $user->id
		];
		$this->printJSON($safe_user);
	}

	public function getUserList()
	{
        $this->checkAuthorizationHeaders();

        $users = new UserCollection();
		$users->fetchAll();
		$data = ['data' => $users->list];
	    $this->printJSON($data);
	}
}
