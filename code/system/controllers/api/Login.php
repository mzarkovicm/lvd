<?php

namespace Lamework\Controller\Api;

require_once(BASEPATH . '/vendor/autoload.php');

use \Lamework\Model\User;
use \Firebase\JWT\JWT;

class Login extends Controller {

	public function __construct() {
		parent::__construct();
	}

	public function login() {
		// Check input
		$request_body = file_get_contents('php://input');
		$data = json_decode($request_body);

		if ($data->grant_type == 'password') {
            if (($data->username == '') || ($data->password == '')) {
                $this->printJSON("All fields are required.", 401);
            } else {
                $username = $data->username;
                $password = md5($data->password);
                $user = new User(array('username' => $username, 'password' => $password));

                if ($user->isAuthenticated()) {
                    $access_token = $this->generateToken([
                        'user_id' => $user->id
                    ]);
                    $refresh_token = $this->generateToken([
                        'username' => $user->username,
                        'password' => $user->password
                    ], true);
                    $this->printJSON(['access_token' => $access_token, 'refresh_token' => $refresh_token]);
                } else {
                    $this->printJSON("Wrong username or password.", 401);
                }
            }
        } else if ($data->grant_type = 'refresh_token') {
            $token_data = $this->checkAuthorizationHeaders(true);
            $user = new User(array('username' => $token_data->username, 'password' => $token_data->password));
            if ($user->isAuthenticated()) {
                $_SESSION['user_id'] = $user->id;
                $access_token = $this->generateToken();
                $refresh_token = $this->generateToken([
                    'username' => $token_data->username,
                    'password' => $token_data->password
                ], true);
                $this->printJSON(['access_token' => $access_token, 'refresh_token' => $refresh_token]);
            } else {
                $this->printJSON("Wrong username or password.", 401);
            }
        }
	}

	private function generateToken($payload = array(), $refresh = false) {
		$tokenId = base64_encode(mcrypt_create_iv(32));
		$issuedAt = time();
		$notBefore = $issuedAt;
		if ($refresh) {
			$expire = $notBefore + 60 * 60 * 24 * 2; // Adding 2 days
		} else {
			$expire = $notBefore + 60 * 60 * 3; // Adding 3 hours
		}
		$serverName = $_SERVER['SERVER_NAME']; // Retrieve the server name
		
		/*
		 * Create the token as an array
		 */
		$data = [
			'iat'  => $issuedAt,         // Issued at: time when the token was generated
			'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
			'iss'  => $serverName,       // Issuer
			'nbf'  => $notBefore,        // Not before
			'exp'  => $expire,           // Expire
			'data' => $payload
		];

		/*
	     * Extract the key, which is coming from the config file. 
	     * 
	     * Best suggestion is the key to be a binary string and 
	     * store it in encoded in a config file. 
	     *
	     * Can be generated with base64_encode(openssl_random_pseudo_bytes(64));
	     *
	     * keep it secure! You'll need the exact key to verify the 
	     * token later.
	     */
	    $secretKey = base64_decode(JWTKEY);

	    /*
	     * Encode the array to a JWT string.
	     * Second parameter is the key to encode the token.
	     * 
	     * The output string can be validated at http://jwt.io/
	     */
		$token = JWT::encode(
	        $data,      //Data to be encoded in the JWT
	        $secretKey, // The signing key
	        'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
	        );

	    return $token;
	}
}
