<?php

namespace Lamework\Controller;

use Lamework\Controller\Core\Controller;

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Not found controller
 */
class Error_404 extends Controller
{
   public function __construct()
   {
      parent::__construct();
   }

   public function index()
   {
      $this->to_tpl['error'] = "Page not found.";

      $this->template = "404";
   }

}
