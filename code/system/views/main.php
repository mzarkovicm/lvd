<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $this->title; ?> </title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/_backend/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="/_backend/css/admin-style.css"/>
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="/_backend/js/main.js"></script>
</head>
<body>
<div id="page">
	<div class="wrapper">
		<div class="fluid-holder admin-page">
			<?php $this->load_template($this->template); ?>
		</div>
	</div>
</div>
</body>
</html>
