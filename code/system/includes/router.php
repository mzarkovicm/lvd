<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * Routing engine 1.0
 * developed for LameWork
 * by Milos Zarkovic mzarkovicm@gmail.com
 * Changelog (07-11-16)
 * - Added REST API support
 */
class Router {
	/**
	 * Stores all defined URI's
	 * @var array
	 */
	private $_uri = array();
	/**
	 * Stores all the functions or the class methods defined
	 * for the URI's
	 * @var array
	 */
	private $_function = array();
	/**
	 * Stores all the HTTP methods
	 * for the URI's
	 * @var array
	 */
	private $_methods = array();

	/**
	 * Stores all the arguments for the class method
	 * @var array
	 */
	private $_arguments = array();

	public $class;
	public $method;
	public $arguments;

	/**
	 * Creates a route that accepts any HTTP method
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function any($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'any');
	}

	/**
	 * Creates a route that accepts only HTTP method POST
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function post($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'post');
	}

	/**
	 * Creates a route that accepts only HTTP method GET
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function get($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'get');
	}

	/**
	 * Creates a route that accepts only HTTP method PUT
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function put($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'put');
	}

	/**
	 * Creates a route that accepts only HTTP method DELETE
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function delete($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'delete');
	}

	/**
	 * Creates a route that accepts only HTTP method OPTIONS
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	public function options($uri = '', $function = '', $arguments = array()) {
		$this->_add($uri, $function, $arguments, 'options');
	}

	/**
	 * Builds a collection of internal URL's to look for
	 * @param string $uri
	 * @param string $function
	 * @param array  $arguments
	 */
	private function _add($uri = '', $function = '', $arguments = array(), $method = '') {
		$this->_uri[] = $uri;

		$this->_arguments[] = $arguments;

		if ($function != '') {
			$this->_function[] = $function;
		}

		if ($method != '') {
			$this->_methods[] = $method;
		}
	}

	private static function checkOrigin()
	{
		$domains = ['http://localhost:8080'];
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			$origin = $_SERVER['HTTP_ORIGIN'];

			if (in_array($origin, $domains)) {
				header('Access-Control-Allow-Origin: ' . $origin);
				header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization');
			}
		}
	}

	/**
	 * Makes the thing run
	 * @return bool
	 * @throws Exception
	 */
	public function resolve()
	{
		$request_uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_SPECIAL_CHARS);
		if (isset($request_uri)) {
			$request_uri = ($request_uri != "/") ? rtrim($request_uri, '/') : $request_uri;
		} else {
			$request_uri = '/';
		}

		if (strstr($request_uri, 'api')) {
			self::checkOrigin();
		}

		$request_method = strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_SPECIAL_CHARS));

		if ($request_method == 'post') {
			if (filter_input(INPUT_POST, '_DELETE', FILTER_SANITIZE_SPECIAL_CHARS)) {
				$request_method = 'delete';
			} else if (filter_input(INPUT_POST, '_PUT', FILTER_SANITIZE_SPECIAL_CHARS)) {
				$request_method = 'put';
			}
		}

		if ($request_method == 'patch') {
			$request_method = 'put';
		}

		$function_arguments = array();

		foreach ($this->_uri as $key => $app_uri) {
			// Replace any wild-cards in registered URI with RegEx
			if (strstr($app_uri, "(:")) {
				$app_uri = str_replace(':any', '.*', str_replace(':num', '[0-9]+', $app_uri));
			}
			if (strstr($app_uri, "(/:")) {
				$app_uri = str_replace(':any', '?.*', str_replace(':num', '?[0-9]+', $app_uri));
			}

			if (
				preg_match("#^$app_uri$#", $request_uri, $matches)
				&& ($this->_methods[$key] == 'any' || ($this->_methods[$key] == $request_method))
			) {
				// Remove the text that matched the full pattern
				// keep only matches
				array_shift($matches);
				foreach ($matches as $match) {
					$function_arguments[] = ltrim($match, '/');
				}
				$use_function = $this->_function[$key];

				/* If $use_function is a class name, instantiate it. */
				if (is_string($use_function)) {
					$args = explode("@", $use_function);

					if (!isset($args[1])) {
						// Set the default class method if not specified.
						$class = $args[0];
						$func = "index";
					} else {
						$class = $args[1];
						$func = $args[0];
					}

					// Add a namespace to the class name
					$class = "\\Lamework\\Controller\\" . $class;

					/* if (!class_exists($class)) {
						throw new Exception('Invalid route: Class "' . $class . '" does not exist.');
					} */

					$this->class = $class;
					$this->method = $func;
					$this->arguments = array_merge($this->_arguments[$key], $function_arguments);
					return true;
				} /* else, presume it is an anonymous function */
				else {
					return call_user_func($use_function);
				}
			}
		}

		$call = new \Lamework\Controller\Error_404();
		$this->class = $call;
		$this->method = "index";
		$this->arguments = $function_arguments;
		return true;
	}
}
