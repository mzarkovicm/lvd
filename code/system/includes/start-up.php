<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

session_start();
// Config
require_once(dirname(dirname(__FILE__)) . '/config.php');
// Engines
require_once(dirname(dirname(__FILE__)) . '/includes/router.php');
// Helpers
require_once(dirname(dirname(__FILE__)) . '/includes/helpers.php');

/**
 * An example of a project-specific implementation.
 *
 * After registering this autoload function with SPL, the following line
 * would cause the function to attempt to load the \Lamework\Controller\Api\Core class
 * from /path/to/project/controllers/api/Core.php:
 *
 *      new \Lamework\Controller\Api\Core;
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register(function ($class) {
    // project-specific namespace prefix
    $prefix = 'Lamework\\';

    // base directory for the namespace prefix
    $base_dir = dirname(dirname(__FILE__)) . '/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    $path_chunks = explode('\\', $relative_class);

    if (count($path_chunks) > 1) {
        // class name
        $class_name = array_pop($path_chunks);
        // class directory
        $base_directory = array_shift($path_chunks) . 's';
        $rest_of_the_path = '';
        if (count($path_chunks) > 0) {
            $rest_of_the_path = implode('/', $path_chunks) . '/';
        }
        $class_directory = strtolower($base_directory . '/' . $rest_of_the_path);
        $class_path = $class_directory . $class_name;
    } else {
        $class_path = $relative_class;
    }

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . $class_path . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require_once($file);
    } else {
        throw new Exception('Invalid route: File "' . $file . '" does not exist.');
    }
});

// Initialize the Core controller
$core = new \Lamework\Controller\Core\Controller();
