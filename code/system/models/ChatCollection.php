<?php

namespace Lamework\Model;

use Lamework\Model\Core\Collection;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ChatCollection extends Collection
{
    protected static $model = 'Chat';

    public function __construct($row = array())
    {
        parent::__construct($row);
    }

}
