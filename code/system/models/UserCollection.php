<?php

namespace Lamework\Model;

use Lamework\Model\Core\Collection;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserCollection extends Collection
{
    protected static $model = 'User';

    public function __construct($row = array())
    {
        parent::__construct($row);
    }

}
