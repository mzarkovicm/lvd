<?php

namespace Lamework\Model;

use Lamework\Model\Core\Model;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends Model
{
    public static $table_name = 'users';
    protected static $fields = array(
        'username' => 'string',
        'password' => 'string',
        'order' => 'int'
    );

    public function __construct($row = array())
    {
        parent::__construct($row);
    }

    /**
     * Check the user
     * @return bool
     */
    public function isAuthenticated()
    {
        return self::getByFieldsValues(array('username','password'), array($this->username, $this->password));
    }

}
