<?php

namespace Lamework\Model\Core;

use \PDO;

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/**
 * Repository model 1.0
 * developed for LameWork
 * by Milos Zarkovic mzarkovicm@gmail.com
 * Changelog (11-02-17)
 * - Added custom field type support
 */
abstract class Repository 
{
	protected static $db;

	public function __construct($row = array()) {
		self::$db = self::connectToDb();
	}

	/**
	 * Connect to a database
	 * @return PDO connection
	 */
	private static function connectToDb() {
		// Database connection
		$db_user = DB_USER;
		$db_pass = DB_PASSWORD;
		$hostname = DB_HOST;
		$db_name = DB_NAME;

		$db = new PDO('mysql:host=' . $hostname . ';dbname=' . $db_name . ';charset=utf8', $db_user, $db_pass);
		// set the PDO error mode to exception
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		return $db;
	}
}
