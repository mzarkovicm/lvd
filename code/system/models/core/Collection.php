<?php

namespace Lamework\Model\Core;

use \PDO;

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

abstract class Collection extends Repository
{
	protected static $model;
	public $list = array();

	public function __construct($row = array())
	{
		parent::__construct();
	}

	/**
	 * Fetch all the records from the database
	 * and save them in the list array
	 * @throws \Exception
	 * @return bool
	 */
	public function fetchAll($orderBy = "order")
	{
		try {
			$model = "\\Lamework\\Model\\" . static::$model;
			// Prepare sql and bind parameters
			$stmt = self::$db->prepare("SELECT * FROM " . $model::$table_name . " ORDER BY `" . $orderBy . "` ASC");
			if ($stmt->execute()) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$this->list[] = new $model($row);
				}
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Fetch all records where field equals value
	 * and save them in list array
	 * @param string $field_name
	 * @param string $value
	 * @param string $orderBy
	 * @return bool
	 * @throws \Exception
	 */
	public function fetchAllByFieldValue($field_name = "", $value, $orderBy = "order")
	{
		try {
            $model = "\\Lamework\\Model\\" . static::$model;
			$stmt = self::$db->prepare("SELECT * FROM " . $model::$table_name . " WHERE `" . $field_name . "`=:value ORDER BY `" . $orderBy . "` ASC");
			$stmt->bindParam(':value', $value, static::getPdoTypeByFieldName($field_name));
			if ($stmt->execute()) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$this->list[] = new $model($row);
				}
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Fetch all records where fields are equal to values
	 * and save them in list array
	 * @param array $fields
	 * @param array $values
	 * @return bool
	 * @throws \Exception
	 */
	public function fetchAllByFieldsValues($fields = array(), $values = array())
	{
		try {
			$i = 0;
			$condition = "";
			$bindParams = array();
			foreach ($fields as $field) {
				if ($i != 0) {
                    $condition .= " AND ";
                }
				if (is_array($values[$i])) {
                    $condition .= "((`" . $field . "`=:" . $field . "1) OR (`" . $field . "`=:" . $field . "2))";
                    $bindParams[':' . $field . '1'] = $values[$i][0];
                    $bindParams[':' . $field . '2'] = $values[$i][1];
                } else {
                    $condition .= "`" . $field . "`=:" . $field;
                    $bindParams[':' . $field] = $values[$i];
				}
				$i++;
			}
            $model = "\\Lamework\\Model\\" . static::$model;
			$sql = "SELECT * FROM " . $model::$table_name . " WHERE " . $condition;
			$stmt = self::$db->prepare($sql);
			if ($stmt->execute($bindParams)) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$this->list[] = new $model($row);
				}
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}
}
