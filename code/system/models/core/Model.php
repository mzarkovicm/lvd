<?php

namespace Lamework\Model\Core;

use \PDO;

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

abstract class Model extends Repository 
{
	protected static $fields;
	public static $table_name;
	protected static $id_field = 'id';
	public static $extra_properties = array();

	public function __construct($row = array()) {
		parent::__construct();
		$this->setFromDb($row);
	}

	protected function setFromDb($row = array())
	{
		$fields = array_merge(array(static::$id_field => 'int'), static::$fields, static::$extra_properties);
		
		foreach ($fields as $field => $type) {
			if (!isset($this->$field)) {
				$this->$field = null;
			}

			if ($type == "int") {
				$this->$field = (int)(($row[$field]) ? $row[$field] : 0);
			} else if ($type == "string") {
				$this->$field = ($row[$field]) ? $row[$field] : "";
			} else if ($type == "date") {
			    if ($row[$field]) {
			        $date = new \DateTime($row[$field]);
                } else {
			        $date = new \DateTime("now");
                }
                $date = $date->format('d.m.Y H:i:s');
				$this->$field =  $date;
			} else if ($type == "bool") {
				$this->$field = ($row[$field] && $row[$field]) ? 1 : 0;
			} else {
				$getFieldValueMethod = 'get' . $field . 'Attribute';
				$fieldValue = $this->$getFieldValueMethod();
				$this->$field = ($row[$field]) ? $row[$field] : $fieldValue;
			}
		}
	}

	/**
	 * Get the record where field equals value
	 * @param string $field_name
	 * @param string $value
	 * @return bool
	 * @throws \Exception
	 */
	public function getByFieldValue($field_name = "", $value) {
		try {
			$stmt = self::$db->prepare("SELECT * FROM " . static::$table_name . " WHERE `" . $field_name . "`=:value");
			$stmt->bindParam(':value', $value, static::getPdoTypeByFieldName($field_name));
			if ($stmt->execute()) {
				$this->setFromDb($stmt->fetch(PDO::FETCH_ASSOC));
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Get the record where all fields equal provided values
	 * @param array $fields
	 * @param array $values
	 * @return bool
	 * @throws \Exception
	 */
	public function getByFieldsValues($fields = array(), $values = array()) {
		try {
			$i = 0;
			$condition = "";
			$bindParams = array();
			foreach ($fields as $field) {
				if ($i != 0) {
					$condition .= " AND ";
				}
				$condition .= "`" . $field . "`=:" . $field;
				$bindParams[':' . $field] = $values[$i];
				$i++;
			}
			$sql = "SELECT * FROM " . static::$table_name . " WHERE " . $condition . " LIMIT 1";

			$stmt = self::$db->prepare($sql);
			// $stmt->bindParam(':value', $value, static::getPdoTypeByFieldName($field_name));
			if ($stmt->execute($bindParams)) {
				$this->setFromDb($stmt->fetch(PDO::FETCH_ASSOC));
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Get the record from the database
	 * based on the provided id
	 * @param int $id
	 * @throws \Exception
	 * @return bool
	 */
	public function getById($id = 0) {
		try {
			$stmt = self::$db->prepare("SELECT * FROM " . static::$table_name . " WHERE `" . static::$id_field . "`=:id");
			$stmt->bindParam(':id', $id, PDO::PARAM_INT);
			if ($stmt->execute()) {
				$this->setFromDb($stmt->fetch(PDO::FETCH_ASSOC));
				return true;
			} else {
				return false;
			}
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Save the object to a database table
	 * @return bool|string
	 * @throws \Exception
	 */
	public function saveToDb() {
		$fields = "(";
		$values = "(";
		$bindParams = array();
		foreach (static::$fields as $field => $type) {
			$fields .= "`" . $field . "`,";
			$values .= ":" . $field . ",";
			if ($type == "date") {
				$bindParams[':' . $field] = $this->$field->format("Y-m-d H:i:s");
			} else if ($type == "bool") {
				$bindParams[':' . $field] = $this->$field ? 1 : 0;
			} else {
				$bindParams[':' . $field] = $this->$field;
			}
		}
		// Remove the trailing commas
		$fields = rtrim($fields, ",");
		$values = rtrim($values, ",");
		$fields .= ")";
		$values .= ")";

		$sql = "INSERT INTO " . static::$table_name . " " . $fields . " VALUES " . $values;

		try {
			$stmt = self::$db->prepare($sql);
			if ($stmt->execute($bindParams))
				return self::$db->lastInsertId();
			else
				return false;
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Update a row in the database table
	 * @return bool|string
	 * @throws \Exception
	 */
	public function updateInDb() {
		$set = "";
		$bindParams = array();
		foreach (static::$fields as $field => $type) {
			$set .= "`" . $field . "`=:" . $field . ",";
			if ($type == "date") {
				$bindParams[':' . $field] = $this->$field->format("Y-m-d H:i:s");
			} else if ($type == "bool") {
				$bindParams[':' . $field] = $this->$field ? 1 : 0;
			} else {
				$bindParams[':' . $field] = $this->$field;
			}
		}
		$set = rtrim($set, ",");
		try {
			$id_field = static::$id_field;
			$stmt = self::$db->prepare("UPDATE " . static::$table_name . " SET " . $set . " WHERE `" . self::$id_field . "`=" . $this->$id_field);
			if ($stmt->execute($bindParams))
				return $this->$id_field;
			else
				return false;
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Delete a row in the database table
	 * @return bool
	 * @throws \Exception
	 */
	public function deleteFromDb() {
		try {
			$stmt = self::$db->prepare("DELETE FROM " . static::$table_name . " WHERE `" . self::$id_field . "`=:id");
			$id_field = static::$id_field;
			$stmt->bindParam(':id', $this->$id_field, PDO::PARAM_INT);
			if ($stmt->execute())
				return true;
			else
				return false;
		} catch (\PDOException $e) {
			throw new \Exception("Error: " . $e->getMessage());
		}
	}

	/**
	 * Return the PDO Constant based on field type
	 * @param string $field
	 * @return int
	 */
	protected static function getPdoTypeByFieldName($field = "") {
		$field_type = static::$fields[$field];
		switch ($field_type) {
			case "int":
				return PDO::PARAM_INT;
				break;
			default:
				return PDO::PARAM_STR;
		}
	}

	public function fetchImages($image_list_property = 'images', $orderBy = 'order')
	{
		//$id_field	= static::$id_field;
		$id	= $this->id;

		// check if the image_list_property exists
		if (!isset(static::$extra_properties[$image_list_property])) {
			return FALSE;
		}

		// store the image classes
		$image_list_class	= static::$extra_properties[$image_list_property];
		// TODO: $image_class		= $image_list_class::$class_single;
		$image_class		= "\\Lamework\\Model\\" . $image_list_class;

		$id_field	= $image_class::$parent_id;

		// get the table
		$table = $image_class::$table_name;

		// get the columns from the image class
		$columns = array_merge( array($image_class::$id_field), array_keys($image_class::$fields) );
		$fields = array();
		foreach ($columns as $field)
			$fields[] = "`{$field}`";
		$fields = implode(',', $fields);

		// create the order clause
		$clause_order = " ORDER BY `" . $orderBy . "` ASC";
		try {
			$stmt = self::$db->prepare(
				"SELECT
					{$fields}
				FROM
					{$table}
				WHERE
					{$id_field} = :id
				{$clause_order}"
			);
			
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			$stmt->execute();

			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$this->$image_list_property->list[] = new $image_class($row);
				//$this->$image_list_property->total++;
			}

			return true;
		} catch (PDOException $e) {
			$message = sprintf('Exception <%s> in file "%s" on line %s: %s', $e->getCode(), $e->getFile(), $e->getLine(), $e->getMessage());
			log_message('error', $message);
		}

		return false;
	}

	public function uploadImage($files_array = array(), $order = false)
	{
		$image_prefix = isset(static::$image_prefix) ? static::$image_prefix : '';

		if ($names = uploadImage(static::$image_versions, $image_prefix, $files_array)) {
			foreach (static::$image_versions as $version => $params) {
				$image_property = "image_{$version}";
				$this->$image_property = $names[$version];
				if ($order) {
					$this->order = $this->getNewOrder(true);
				}
			}
			return true;
		} else {
			return false;
		}
	}

	public function getNewOrder($has_parent = false)
	{
		$where = '';
		if ($has_parent) {
			$parent_id_name = static::$parent_id;
			$parent_id_value = $this->$parent_id_name;
			$where = ' WHERE `' . $parent_id_name . '`=:id';
		}
		
		try {
			$stmt = self::$db->prepare('SELECT MAX(`order`) AS max FROM ' . static::$table_name . $where);
			if ($has_parent) {
				$stmt->bindParam(':id', $parent_id_value, PDO::PARAM_INT);
			}
			if ($stmt->execute()) {
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				if ($row['max'] != null) {
					return $row['max'] + 1;
				}
			}
			return 0;
		} catch (\PDOException $e) {
			throw new \Exception('Error: ' . $e->getMessage());
		}
	}

	public function reorder($removed_order = 1, $has_parent = false)
	{
		$and = '';
		if ($has_parent) {
			$parent_id_name = static::$parent_id;
			$parent_id_value = $this->$parent_id_name;
			$and = ' AND `' . $parent_id_name . '`=:id';
		}

		try {
			$stmt = self::$db->prepare('UPDATE ' . static::$table_name . ' SET `order` = `order` - 1 WHERE `order` > :removed' . $and);
			$stmt->bindParam(':removed', $removed_order, PDO::PARAM_INT);
			if ($has_parent) {
				$stmt->bindParam(':id', $parent_id_value, PDO::PARAM_INT);
			}
			if ($stmt->execute()) {
				return true;
			}
			return false;
		} catch (\PDOException $e) {
			throw new \Exception('Error: ' . $e->getMessage());
		}
	}

	/**
	 * NEEDS REFACTORING
	 * but it works for now
	 */
	public function setNewOrder($item_list = array(), $column_id = 0, $order_column = '', $group_column = '')
	{
		if (!is_array($item_list)) {
			$item_list = explode(',', $item_list);
		}

		if (empty($item_list)) {
			return true;
		}

		// $class = static::$class_single;

		// set table name
		$table = static::$table_name;

		$id_column = static::$id_field;

		// set order column
		// $default_order_column = isset( $class::$default_order_column ) ? $class::$default_order_column : '';
		$order_column = 'order';
		
		if (!$order_column) {
			return false;
		}

		if ($column_id != -1) {
			// set group column
			$default_group_column = static::$parent_id;
			$group_column = $default_group_column;
			$clause_where = $group_column ? "AND `{$group_column}` = :{$group_column}" : '';

			// set column id
			if ($group_column) {
				// $column_id = $this->$id_column;
			}
		} else {
			$group_column = '';
			$clause_where = '';
		}

		try {
			self::$db->beginTransaction();

			$stmt = self::$db->prepare("UPDATE `{$table}`
										SET `{$order_column}` = :order
										WHERE `{$id_column}` = :id
											{$clause_where}");

			$stmt->bindParam(':order', $order, PDO::PARAM_INT);
			$stmt->bindParam(':id', $id, PDO::PARAM_INT);
			if ($group_column) {
				$stmt->bindValue(":{$group_column}", $column_id, PDO::PARAM_INT);
			}
			$order = 0;
			foreach ($item_list as $order => $id) {
				$stmt->execute();
				$order++;
			}
			
			self::$db->commit();

			return true;
		} catch (PDOException $e) {
			self::$db->rollBack();
			$message = sprintf('Exception <%s> in file "%s" on line %s: %s', $e->getCode(), $e->getFile(), $e->getLine(), $e->getMessage());
			log_message('error', $message);
		}

		return false;
	}
}
