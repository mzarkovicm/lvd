<?php

namespace Lamework\Model;

use Lamework\Model\Core\Model;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends Model
{
    public static $table_name = 'chats';
    protected static $fields = array(
        'sender_id' => 'int',
        'receiver_id' => 'int',
        'chat' => 'string',
        'read' => 'bool',
        'date_created' => 'date'
    );

    static public $extra_properties = array(
        'sender' => 'User',
        'receiver' => 'User'
    );

    public function __construct($row = array())
    {
        parent::__construct($row);
    }

    public function getSenderAttribute()
    {
        $user = new User();
        $user->getById($this->sender_id);
        return $user;
    }

    public function getReceiverAttribute()
    {
        $user = new User();
        $user->getById($this->receiver_id);
        return $user;
    }
}
