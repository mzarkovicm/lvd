<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$core->route->get('/', 'pages@Admin_pages');

// Public Ajax
$core->route->get('/api/auth-user', 'getAuthorizedUser@Api\User');
$core->route->post('/api/login', 'login@Api\Login');
$core->route->post('/api/login/refresh', 'login@Api\Login');
// Chat URLs
$core->route->get('/api/v1/user-list', 'getUserList@Api\User');
$core->route->post('/api/v1/get-user-conversation', 'getUserConversationById@Api\Chat');
$core->route->post('/api/v1/save-chat', 'getUserConversationById@Api\Chat');

// CORS
$core->route->options('/api(/:any)', 'index@AjaxController');
// AJAX 404
$core->route->any('/api(/:any)', 'notFound@AjaxController');

// Not found
$core->route->any('/(:any)', 'index@Error_404');
