export const apiDomain = 'http://localhost/'
export const userUrl = apiDomain + 'api/auth-user'
export const loginUrl = apiDomain + 'api/login'
export const userListUrl = apiDomain + 'api/v1/user-list'
export const getUserConversationUrl = apiDomain + 'api/v1/get-user-conversation'
export const saveChatMessageUrl = apiDomain + 'api/v1/save-chat'

export const getHeader = function () {
  const userData = JSON.parse(window.localStorage.getItem('authUser'))
  const headers = {
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + userData.access_token
  }
  return headers
}
