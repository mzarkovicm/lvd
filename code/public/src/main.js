// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import store from './store'

import App from './App'

// Pages
import LoginPage from './pages/LoginPage'
import DashboardPage from './pages/DashboardPage'
import ChatPage from './pages/ChatPage'

import {loginUrl} from './config'

Vue.use(VueResource)
Vue.use(VueRouter)

// Define root component
Vue.component('application', App)

const routes = [
  {
    path: '/',
    component: LoginPage,
    name: 'home'
  },
  {
    path: '/dashboard',
    component: DashboardPage,
    name: 'dashboard',
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/chat',
    component: ChatPage,
    name: 'chat',
    meta: {
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const authUser = JSON.parse(window.localStorage.getItem('authUser'))
    if (authUser && authUser.access_token) {
      next()
    } else {
      next({name: 'home'})
    }
  }
  next()
})

Vue.http.interceptors.push(function (request, next) {
  // continue to next interceptor
  next(function (response) {
    if (response.status === 401 && response.body === 'Expired access token') {
      console.log('Expired access token')
      const userData = JSON.parse(window.localStorage.getItem('authUser'))
      const headers = {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + userData.refresh_token
      }
      const postData = {
        grant_type: 'refresh_token'
      }
      const authUser = {}
      Vue.http.post(loginUrl + '/refresh', postData, {headers: headers}).then(response => {
        if (response.status === 200) {
          console.log('new token', response)

          authUser.access_token = response.body.access_token
          authUser.refresh_token = response.body.refresh_token
          window.localStorage.setItem('authUser', JSON.stringify(authUser))
        }
      }).catch(function (error) {
        console.log('Expired refresh token', error)
        store.dispatch('clearAuthUser')
        window.localStorage.removeItem('authUser')
        router.push({name: 'home'})
      })
    }
  })
})

new Vue({
  router,
  store,
  template: '<application/>'
}).$mount('#app-target')
